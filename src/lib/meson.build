gnome = import('gnome')

hackernews_glib_gir_namespace = 'Hn'
hackernews_glib_gir_nsversion = '1.0'

inc = include_directories('.')

hackernews_glib_sources = [
  'hn-api.c',
  'hn-client.c',
  'hn-item.c',
  'hn-node.c',
  'hn-api-endpoint.c',
  'hn-endpoints.c',
  'hn-account-manager.c',
]

hackernews_glib_headers = [
  'hackernews-glib.h',
  'hn-api.h',
  'hn-client.h',
  'hn-item.h',
  'hn-node.h',
  'hn-api-endpoint.h',
  'hn-endpoints.h',
  'hn-account-manager.h',
]

libname = 'hackernews-glib'
hackernews_glib_libdir = get_option('libdir')
hackernews_glib_includedir = join_paths(get_option('includedir'), libname)

hn_enums = gnome.mkenums_simple ('hn-item-enums', sources: ['hn-item.h'])

version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]
api_version = MAJOR_VERSION + '.' + MINOR_VERSION

version_conf = configuration_data()
version_conf.set('VERSION', meson.project_version())
version_conf.set('MAJOR_VERSION', MAJOR_VERSION)
version_conf.set('MINOR_VERSION', MINOR_VERSION)
version_conf.set('MICRO_VERSION', MICRO_VERSION)

configure_file(
  input: 'hn-version.h.in',
  output: 'hn-version.h',
  configuration: version_conf,
  install: true,
  install_dir: hackernews_glib_includedir,
)

hackernews_glib_deps = [
  gio_dep,
  dependency('libsoup-2.4'),
  dependency('json-glib-1.0'),
]

hackernews_glib_lib = library('hackernews-glib-' + api_version,
  hackernews_glib_sources + hn_enums,
  dependencies: hackernews_glib_deps,
  install: true,
  install_dir: hackernews_glib_libdir,
)

hackernews_glib_dep = declare_dependency(
              sources: hn_enums,
         dependencies: hackernews_glib_deps,
            link_with: hackernews_glib_lib,
  include_directories: include_directories('.'),
)

install_headers(hackernews_glib_headers, subdir: libname)

pkg = import('pkgconfig')

pkg.generate(
  description: 'A shared library for the hackernews API',
    libraries: hackernews_glib_lib,
         name: 'hackernews-glib',
     filebase: 'hackernews-glib-' + api_version,
      version: meson.project_version(),
      subdirs: 'hackernews-glib',
     requires: 'glib-2.0',
  install_dir: join_paths(get_option('libdir'), 'pkgconfig')
)

#################
# Introspection #
#################

# if get_option('with_introspection')

#   hackernews_glib_gir = gnome.generate_gir(
#     hackernews_glib_lib,
#     extra_args: ['-v'],
#     sources: hackernews_glib_sources + hackernews_glib_headers,
#     header: 'hackernews-glib.h',
#     dependencies: hackernews_glib_deps,
#     namespace: hackernews_glib_gir_namespace,
#     nsversion: hackernews_glib_gir_nsversion,
#     identifier_prefix: hackernews_glib_gir_namespace,
#     install: true,
#     includes: ['Soup-2.4', 'GObject-2.0'],
#     symbol_prefix: ['hn'],
#   )

#   vapidir = join_paths(get_option('datadir'), 'vala', 'vapi')

#   if get_option('with_vapi')

#     hackernews_glib_vapi = gnome.generate_vapi('hackernews-glib-' + api_version,
#             sources: hackernews_glib_gir[0],
#            packages: [ 'gio-2.0', 'libsoup-2.4' ],
#             install: true,
#         install_dir: vapidir,
#       metadata_dirs: [ meson.current_source_dir() ],
#     )

#   endif
# endif
