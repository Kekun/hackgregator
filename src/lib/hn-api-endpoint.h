/* hn-api-endpoint.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define HN_TYPE_API_ENDPOINT (hn_api_endpoint_get_type ())

G_DECLARE_INTERFACE (HnApiEndpoint, hn_api_endpoint, HN, API_ENDPOINT, GObject)

struct _HnApiEndpointInterface
{
  GTypeInterface parent;

  gchar *(*get_name)       (HnApiEndpoint *self);
  gchar *(*get_identifier) (HnApiEndpoint *self);
  gchar *(*get_uri)        (HnApiEndpoint *self);
};

gchar *hn_api_endpoint_get_name (HnApiEndpoint *self);
gchar *hn_api_endpoint_get_identifier (HnApiEndpoint *self);
gchar *hn_api_endpoint_get_uri (HnApiEndpoint *self);

G_END_DECLS
