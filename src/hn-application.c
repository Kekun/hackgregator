/* hn-application.c
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "hn-application"

#include "hn-application.h"
#include "hn-application-window.h"
#include "hn-debug.h"

struct _HnApplication
{
  GtkApplication parent_instance;
};

static void           hn_application_show_shortcuts              (GSimpleAction *simple,
                                                                  GVariant      *parameter,
                                                                  gpointer       user_data);

static void           hn_application_show_about                  (GSimpleAction *simple,
                                                                  GVariant      *parameter,
                                                                  gpointer       user_data);

static void           hn_application_quit                        (GSimpleAction *simple,
                                                                  GVariant      *parameter,
                                                                  gpointer       user_data);

G_DEFINE_TYPE (HnApplication, hn_application, GTK_TYPE_APPLICATION)

static const GActionEntry hn_application_entries[] = {
  { .name = "shortcuts", .activate = hn_application_show_shortcuts },
  { .name = "about", .activate = hn_application_show_about },
  { .name = "quit", .activate = hn_application_quit }
};

HnApplication *
hn_application_new (void)
{
  return g_object_new (HN_TYPE_APPLICATION,
                       "resource-base-path", "/de/gunibert/Hackgregator",
                       "application-id", "de.gunibert.Hackgregator",
                       NULL);
}

static void
hn_application_set_custom_style (HnApplication *self)
{
  GdkScreen *screen = NULL;
  GtkCssProvider *provider = NULL;

  screen = gdk_screen_get_default ();
  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (provider, "/de/gunibert/Hackgregator/custom.css");
  gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

static void
hn_application_activate (GApplication *app)
{
  HnApplication *self = HN_APPLICATION (app);
  GtkWindow *window = NULL;

  hn_application_set_custom_style (self);

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  if (window == NULL)
    {
      window = GTK_WINDOW (hn_application_window_new (self));
    }

  gtk_window_present (window);
}

static void
hn_application_startup (GApplication *app)
{
  HnApplication *self = HN_APPLICATION (app);

  /* add actions */
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   hn_application_entries,
                                   G_N_ELEMENTS (hn_application_entries),
                                   self);

  /* add accelerators */
  const gchar *accels_quit[] = {"<Control>q", NULL};
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.quit", accels_quit);
  const gchar *accels_shortcuts[] = {"<Control>question", NULL};
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.shortcuts", accels_shortcuts);

  G_APPLICATION_CLASS (hn_application_parent_class)->startup (app);
}

static void
hn_application_class_init (HnApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = hn_application_activate;
  app_class->startup = hn_application_startup;
}

static void
hn_application_init (HnApplication *self)
{
}

static void
hn_application_show_shortcuts (GSimpleAction *simple,
                               GVariant      *parameter,
                               gpointer       user_data)
{
  GtkBuilder *builder = gtk_builder_new_from_resource ("/de/gunibert/Hackgregator/hn-shortcuts-window.ui");
  GtkWidget *window = (GtkWidget *)gtk_builder_get_object (builder, "window");

  gtk_window_present (GTK_WINDOW (window));
}

static void
hn_application_show_about (GSimpleAction *simple,
                           GVariant      *parameter,
                           gpointer       user_data)
{
  HnApplication *self = HN_APPLICATION (user_data);

  HN_ENTRY;

  GtkWindow *window = gtk_application_get_active_window (GTK_APPLICATION (self));

  static const gchar *authors[] = {
    "Günther Wagner <info@gunibert.de>",
    NULL
  };

  static const gchar *artists[] = {
    "Robert Martinez <mail@mray.de>",
    NULL,
  };

  gtk_show_about_dialog (window,
                         "program-name", "Hackgregator",
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "authors", authors,
                         "artists", artists,
                         "copyright", "Copyright \xC2\xA9 2019-2020 Günther Wagner",
                         "logo-icon-name", "de.gunibert.Hackgregator",
                         NULL);

  HN_EXIT;
}

static void
hn_application_quit (GSimpleAction *simple,
                     GVariant      *parameter,
                     gpointer       user_data)
{
  HnApplication *self = HN_APPLICATION (user_data);
  GtkWindow *window = gtk_application_get_active_window (GTK_APPLICATION (self));
  gtk_widget_destroy (GTK_WIDGET (window));
}
